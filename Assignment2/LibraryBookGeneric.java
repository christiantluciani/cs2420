/*
 * Ben Broadhead
 * Christian Luciani
 * CS 2420-001
 * 1/30/15
 */

package assignment2;

import java.util.GregorianCalendar;

public class LibraryBookGeneric<Type> extends Book {

	private Type holder;

	private GregorianCalendar dueDate;

	public LibraryBookGeneric(long _isbn, String _author, String _title) {
		super(_isbn, _author, _title);
		holder = null;
		dueDate = null;
	}

	/**
	 * @return the holder
	 */
	public Type getHolder() {
		return this.holder;
	}

	/**
	 * @return the due date
	 */
	public GregorianCalendar getDueDate() {
		return this.dueDate;
	}

	/**
	 * Sets the holder of the libraryBook object to a name or phone number
	 * (string). Sets the due date of the libraryBook object to a specific date
	 * (GregorianCalendar).
	 */
	public boolean checkOut(Type h, int m, int d, int y) {
		if (holder == null) {
			holder = h;
			dueDate = new GregorianCalendar(y, m, d);
			return true;
		}
		return false;
	}

	/**
	 * Sets the holder of the libraryBook object to null. Sets the due date of
	 * the libraryBook object to null.
	 */
	public boolean checkIn() {
		if (holder != null) {
			holder = null;
			dueDate = null;
			return true;
		}
		return false;
	}

}
